# 安装docker

> https://docs.docker.com/engine/install/debian/

# 配置权限
```shell
# 当前用户加入docker组
sudo usermod -aG docker $USER
```

# 下载镜像
```shell
docker pull archlinux
docker run --privileged --name arch -h arch -dt -p 7022:22 archlinux
```

# 创建镜像
```shell
# 查看CONTAINER_ID
docker ps

# 创建新的镜像
sudo docker commit [CONTAINER_ID] [new_image_name]

# 查看创建的镜像
docker images
```

# 删除镜像
```shell
docker image rm image_name
```

# 启动demon实例

```shell
docker run -dt dockerImage #detach tty
```

# 启动容器内部服务

```shell
docker exec -d arch /usr/sbin/sshd
```

# FAQ
## operation not permitted
```shell
# --name container name
# -h container hostname
# -p local_port:container_port
# --privileged Give extended privileges to this container. A "privileged" container is given access to all devices
docker run --privileged --name arch -h arch -dt -p 7022:22 archlinux
```

## 给运行的实例分配IP

以下命令通过启动docker容器后，观察iptbales的nat表实现的,自己胡乱操作不常见
```shell
# command 1
iptables -t nat -A  DOCKER -p tcp --dport 7022 -j DNAT --to-destination 172.17.0.2:22
# command 2
iptables --wait -t filter -A DOCKER ! -i docker0 -o docker0 -p tcp -d 172.17.0.2 --dport 22 -j ACCEPT
```