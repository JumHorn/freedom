# change repo
* edit /etc/pacman.d/mirrorlist
```conf
Server = http://mirrors.ustc.edu.cn/archlinux/$repo/os/$arch
```
```shell
echo "Server = http://mirrors.ustc.edu.cn/archlinux/\$repo/os/\$arch" > /etc/pacman.d/mirrorlist
```

# ssh
1. 启动sshd服务
```shell
# 注意文件权限和所属用户和组
/usr/sbin/sshd -h /root/.ssh/id_rsa
```

2. 避免密码登陆
	非常重要,我的密码设置的简单,防止被暴力破解
```shell
# vim /etc/ssh/sshd_config
PasswordAuthentication no
```

# timezone(时区)
```shell
# TZ=Asia/Shanghai不行
# 不是环境变量,其它程序不可见
export TZ=Asia/Shanghai
```

# must install tools
* net command
```shell
pacman -S openssh # ssh
pacman -S net-tools
pacman -S inetutils
pacman -S dnsutils
pacman -S tmux #离线任务,分屏必备神器
pacman -S htop #top的升级版本
```

* C/C++ dev
	可选
```shell
pacman -S base-devel
pacman -S cmake
pacman -S clang
```

* others
	可选
```shell
pacman -S networkmanager # for nmcli
```

# man page

pacman.conf from the Docker image has got 'NoExtract = usr/share/man/* usr/share/info/*' in it and that's why man-pages is effectively not installed. Remove this line and try again.
```shell
pacman -Sy --noconfirm man man-db man-pages
```

# port mapping

> https://github.com/fatedier/frp

frps.ini
```ini
[common]
bind_port = 7317
```

frpc.ini
```ini
[common]
server_addr = 81.69.226.177
server_port = 7317

[redis]
type = tcp
local_ip = 127.0.0.1
local_port = 6379
remote_port = 16379

[ssh]
type = tcp
local_ip = 127.0.0.1
local_port = 22
remote_port = 7022
```

# FAQ

1. 启动sshd
```shell
/usr/bin/sshd -f /path/to/sshd_config -h /path/to/hostkeys
```