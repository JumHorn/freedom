# macOS Big Sur

# HomeBrew
## brew和apt/yum等命令相似是包管理器,负责安装软件
1. 安装brew
```shell
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
2. 小技巧

brew install时第一步先update homebrew由于网速慢,可以ctrl+c跳过这一步 \
也可以设置环境变量
```shell
# 在profile.d/aliases.sh中已经设置
HOMEBREW_NO_AUTO_UPDATE=1 brew install
```

3. 解决路径权限不足问题

~~sudo chown -R $(whoami) $(brew --prefix)~~

## 常用软件
```shell
# redis
brew install redis
# mysql client
brew install mysql-client
# 包含(ftp,telnet)
brew install inetutils
# nmap
brew install nmap
# htop
brew install htop
# tmux
brew install tmux
# 远程共享文件
brew install --cask osxfuse
brew install sshfs
# wireshark
brew install --cask wireshark
# libreoffice(alternatives to wps)
brew install --cask libreoffice
```

# GNU神器
# 安装GNU Bash
1. install GNU Bash
```shell
# 安装Bash
brew install bash
# 查看Bash
which -a bash

# 添加新Bash
sudo vim /etc/shells
# 添加新bash到文件尾部
/usr/local/bin/bash

# 设置默认Bash
chsh -s /usr/local/bin/bash
# 查看Bash版本
bash --version
```
2. install Bash Tools
```shell
brew install coreutils findutils gnu-tar gnu-sed gawk gnutls gnu-indent gnu-getopt grep
# 添加到环境变量
if type brew &>/dev/null; then
	HOMEBREW_PREFIX=$(brew --prefix)
	# gnubin; gnuman
	for d in ${HOMEBREW_PREFIX}/opt/*/libexec/gnubin; do export PATH=$d:$PATH; done
	# I actually like that man grep gives the BSD grep man page
	#for d in ${HOMEBREW_PREFIX}/opt/*/libexec/gnuman; do export MANPATH=$d:$MANPATH; done
fi
```

3. 设置alias

copy aliases.sh 到/etc/profile.d目录下
```shell
alias ls='ls -F --color=auto'
alias grep='grep --color=auto'
alias ssh='ssh -o "ServerAliveInterval 30" -o "ServerAliveCountMax 100"'
# mac only
alias ldd='otool -L'
alias brew='HOMEBREW_NO_AUTO_UPDATE=1 brew'
```

4. 设置变量

目前Mac下将这些直接写在~/.bashrc文件中

copy variables.sh 到/etc/profile.d目录下
```shell
# PS1
PS1="\u:\w\$(__git_ps1)\$ "

# Debian
debian="jumhorn@192.168.31.232"

# CentOS
centos="81.69.226.177"

# Arch
# local arch
# export arch="root@192.168.31.68 -p 7022"
# reverse tunnel arch
arch="root@192.168.31.232 -p 7022"

# proxy
# all_proxy="http://127.0.0.1:7890"
```

# git配置

## 用户配置
```shell
git config --global user.name JumHorn
git config --global user.email JumHorn@gmail.com
```
## ssh配置
1. 生成公钥密钥
```shell
ssh-keygen -t rsa -C "JumHorn@gmail.com"
```

2. 复制id_rsa.pub内容

文件在home目录下 ~/.ssh/id_rsa.pub
```shell
chmod 700 ~/.ssh/
chown -R $(USER) ~/.ssh
chgrp -R $(USER) ~/.ssh
chmod 644 id_rsa.pub
chmod 600 id_rsa
```

**现在采用备份私钥公钥的方式避免修改**

## git命令行提示
1. git-prompt.sh

复制git源码目录下的git/contrib/completion/git-prompt.sh到/etc/profile.d目录下 \
在/etc/profile内添加如下
```shell
if [ -d /etc/profile.d ]; then
	for i in /etc/profile.d/*.sh; do
		if [ -r $i ]; then
			. $i
		fi
	done
	unset i
fi
```
2. 修改.bashrc
```shell
# 不带颜色
export PS1="\u:\W\$(__git_ps1)\$ "
```

# vim
1. 下载vim并编译
> https://github.com/vim/vim
```shell
./configure --enable-python3interp
```

2. 自用vim配置
> https://github.com/JumHorn/vim

# 终端

1. 终端颜色调整

	terminal==>preference==>profiles==>ANSI Colors调整对应的颜色

2. 关闭终端闪烁

	terminal==>preference==>profiles==>advance==>visual bell

3. 关闭终端提示音

	terminal==>preference==>profiles==>advance==>audible bell

4. 中文乱码

	现在安装系统默认英文，没有这个问题了 \
~~add the following to ~/.bash_profile~~
```shell
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
```

# gdb配置
太麻烦了，不想这么深入的学习MacOS,mac能用即可
## lldb

lldb大部分命令和gdb类似

## vscode+cpptools

直接安装vscode使用vscode支持的debug就行了
使用自带的clang++

### 调试快捷键F11冲突 disable F11 show desktop
1. Go to System Preferences -> Keyboard -> Shortcuts.
2. Uncheck the "Show Desktop F11" option.

# 日常软件
1. chrome
2. vscode
3. clash
4. 直接系统添加中文拼音输入法(caps lock切换输入法)
5. Foxit Reader

# 系统设置
## 禁止更新
1. System preference
禁止software update自动更新
2. app store preference
禁止所有应用自动更新

## preference
1. 左上角apple图标
2. 所有程序的设置菜单栏设置
3. 右键空白dock栏 dock preference

## 手势设置
system==>preference==>trackpad

1. three finger drag
	system==>preference==>accessbility==>mouse & trackpad==>trackpad options==>enable drag==>three finger drag

## 键盘设置
1. 设置键盘背光灯

	system==>preference==>keyboard==>adjust keyboard brightness in low light \
	按住F5(或者fn+F5)将亮度减少到0
2. 把F1-12设置为标准功能键

	system==>preference==>keyboard==>use F1,F2

## 用户提升权限
```shell
enable root account
```
## 更新系统命令行失效
```shell
xcode-select --install
```
## 删除系统自带壁纸
1. /System/Library/Desktop Pictures(系统目录)
2. /Library/Desktop Pictures(用户目录)
## 取消权限提示
```shell
sudo DevToolsSecurity --enable
```
## Finder默认文件夹
	finder==>preference==>general==>new finder window show
# FAQ
## readonly file system
1. sudo mount -uw /
2. 1步骤无效，则获取SIP权限

## cannot verify xxx free from malware
1. System Preferences
2. Security & Privacy
3. General
4. Open Anyway

## real /tmp of mac
```shell
ls -l /tmp
/private/tmp
```