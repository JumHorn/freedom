# termux

# change source list

* edit source.list

```shell
# 编辑 /data/data/com.termux/files/usr/etc/apt/sources.list 文件
cd /data/data/com.termux/files/usr/etc/apt

echo "deb https://mirrors.ustc.edu.cn/termux/apt/termux-main stable main" >
sources.list

apt upgrade
```

* using command

> termux-change-repo

# ssh login

1. install and start openssh
```shell
pkg install openssh # install
sshd				# start
```

2. find ssh port
```shell
netstat -antp | grep sshd
```

3. show current user
```shell
whoami
```

4. connect
```shell
ssh user@ip -p port
```

# install tools
* pkg install vim
* pkg install htop
* pkg install nmap

# PRoot
1. install proot
```shell
pkg install proot
pkg install proot-distro
```
2. install linux distro
```shell
proot-distro list
proot-distro install debian
proot-distro login debian
```

# startup
1. create startup.sh file
```shell
# ssh
sshd
# redis
redis-server &> /dev/null &
# mysql
mariadbd &> /dev/null &
```
2. copy it to /etc/profile.d/

# common sense
1. $PREFIX 用该环境变量可以查看根目录
