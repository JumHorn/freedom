# Debian 11(bullseye)
# Debian installaton

# wifi
1. 修改配置文件

	配置文件连接wifi,修改/etc/network/interfaces文件,wlan0表示无线网卡名
	现在更好的做法是在/etc/network/interfaces.d目录下做修改

```shell
auto wlan0
iface wlan0 inet dhcp
pre-up ip link set wlan0 up
pre-up iwconfig wlan0 essid ssid
wpa-ssid ssid
wpa-psk password
```

2. 启动wifi

```shell
ifup wlan0
```


# sudo
	添加超级用户权限
	现在更好的做法是在/etc/sudoers.d目录下做修改
	细节查看该目录下的readme文件(主要是注意权限和文件名)
```shell
apt install sudo
chmod +w /etc/sudoers
vim /etc/sudoers #添加下面一行为用户增加sudo权限
$USER ALL=(ALL:ALL) NOPASSWD:ALL
chmod -w /etc/sudoers
```

# enable non-free source
* edit source.list
1. vim /etc/apt/source.list
2. add the following

> deb http://mirrors.ustc.edu.cn/debian/ bullseye main non-free contrib

> deb-src http://mirrors.ustc.edu.cn/debian/ bullseye main non-free contrib

* apt update

用**aptitude install package**解决包依赖问题

# 开机不用进入系统选择界面
1. vim /etc/default/grub
```shell
GRUB_DEFAULT=0
GRUB_TIMEOUT=0
GRUB_HIDDEN_TIMEOUT=0
GRUB_HIDDEN_TIMEOUT_QUIET=true
GRUB_CMDLINE_LINUX_DEFAULT="quiet"
GRUB_CMDLINE_LINUX=""
```

2. sudo update-grub

# 防止合盖休眠(suspend)
* vim /etc/systemd/logind.conf
```shell
HandleLidSwitch=ignore
```

# timezone(时区)
```shell
# timedatectl list-timezones
sudo timedatectl set-timezone Asia/Shanghai
```

# 避免密码登陆
	非常重要,我的密码设置的简单,防止被暴力破解
* vim /etc/ssh/sshd_config
```shell
PasswordAuthentication no
```

# enable auto mount
1. vim /etc/rc.local

	use fdisk -l to show mount device

```shell
#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.

# auto mount device
sudo mount /dev/sdb5 /media/D
sudo mount /dev/sdb6 /media/E
sudo mount /dev/sdb7 /media/F

exit 0
```
2. chmod +x /etc/rc.local

# 常用设置

1. alias

copy aliases.sh 到/etc/profile.d目录下
```shell
alias ls='ls -F --color=auto'
alias grep='grep --color=auto'
alias ssh='ssh -o "ServerAliveInterval 30" -o "ServerAliveCountMax 100"'
```

# 两大神器
# vim

1. 下载vim并编译
> https://github.com/vim/vim
```shell
sudo apt install libncurses-dev
./configure --enable-python3interp
```

2. 自用vim配置
> https://github.com/JumHorn/vim

# git
```shell
apt install git
```
## 用户配置
```shell
git config --global user.name JumHorn
git config --global user.email JumHorn@gmail.com
```
## ssh配置

	对于新的机器可以这样使用，旧的机器已经弃用这种方法了
**现在保存文件id_rsa id_rsa.pub密钥对,并恢复到.ssh目录下(注意权限即可)**
1. 生成公钥密钥
```shell
ssh-keygen -t rsa -C "JumHorn@gmail.com"
```

2. 复制id_rsa.pub内容

文件在home目录下 ~/.ssh/id_rsa.pub

* copy到github profile settings
* copy到gitlab profile settings
* copy到vultr
* 腾讯云

## bash显示git分支配置
1. git-prompt.sh

复制git源码目录下的git/contrib/completion/git-prompt.sh到/etc/profile.d目录下

2. 修改.bashrc
	vim ~/.bashrc
```shell
# 不带颜色,注意单双引号区别
export PS1="\u@\h \w\$(__git_ps1)\$ "
```

```shell
# 带颜色,注意单双引号区别
export PS1="\[\033[01;32m\]\u@\h\[\033[00m\] \[\033[01;34m\]\w\[\033[00m\]\[\033[00;32m\]\$(__git_ps1)\[\033[00m\]\$ "
```

3. 生效
```shell
. ~/.profile
# 或者
source ~/.profile
```

# must install tools
* common
```shell
sudo apt install network-manager # nmcli
sudo apt install net-tools
sudo apt install dnsutils
sudo apt install tmux #离线任务,分屏必备神器
sudo apt install htop #top的升级版本
sudo apt install iftop #网络版top
sudo apt install brightnessctl #调节亮度
sudo apt install amixer #调节音量
```

* database
```shell
sudo apt install redis
sudo apt install mariadb-server # mysql
```

* C/C++ dev
```shell
sudo apt install build-essential
sudo apt install cmake
```

# Graphic User
## gnome-tweaks设置
1. auto hide top bar
```shell
apt install gnome-shell-extensions
apt install gnome-tweak-tool
sudo apt install gnome-shell-extension-autohidetopbar
```
> https://github.com/mlutfy/hidetopbar

搜索 tweak tool来配置gnome-shell-extensions
在extension中配置hidetopbar启用第一个选项

2. set dark mode

在appearance设置Adwaita-dark

3. 关闭时间同步

在settings下的data&time选项==> automatic datatime

4. 设置ctl+alt+T快捷键

settings==>keyboard==>custom shortcuts
```shell
name terminal
command gnome-terminal
shortcut ctrl+alt+t
```
## 常用软件
* google拼音

```shell
sudo apt install fcitx fcitx-googlepinyin
```
设置英文默认输入,Fcitx config中keyboard-English调整到Google Pinyin之前

一次不行就多调几次，或者重启

* chrome浏览器

可以用offline installer
必装应用 **DevDocs**
chrome app **DevDocs**

* terminal/gnome-vim

* visual studio code

设置title样式
```vscode
window.title custom
```

* Foxit reader(PDF)

不要用root用户安装,安装完在设置中关闭启动检查更新
alternative 1)LibreOffice Draw 2)wps

* qv2ray(v2ray)

将v2ray核心文件v2ray和v2ctl复制到qv2ray的config目录下
> https://qv2ray.net/en/getting-started/step2.html#download-v2ray-core-files

* libreoffice/wps

* octave

* geogebra


# FAQ

* 制作系统镜像盘
```shell
sudo umount /dev/sdX #U盘(可以使用lsblk命令查找)
sudo dd if=/path/to/debian.iso of=/dev/sdX bs=4M
sudo sync # 确保完成写入U盘，数据完全从内存写到U盘
```
* 系统镜像盘中有带wifi的**rescue mode**

* restore .bashrc file

> http://www.linfo.org/etc_skel.html

```shell
# defautl .bashrc file for useradd to use
cp /etc/skel/.bashrc ~/
```

* install realtek(声卡驱动)
```shell
apt install firmware-realtek
update-initramfs -u
```

* install bluetooth driver(蓝牙驱动)
```shell
apt install firmware-atheros
```